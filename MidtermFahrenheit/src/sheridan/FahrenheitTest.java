/**
 * 
 */
package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author bhatt harshal [991546101]
 *
 */
public class FahrenheitTest {

	@Test
	public void testFahrenheitRegular() {
		int fahrenheit = 32;
		int celsius = (int) ((5 * (fahrenheit - 32)) / 9);
		assertEquals(0, celsius);
	}

	@Test
	public void testFahrenheitExceptional() {
		int fahrenheit = -12;
		int celsius = (int) ((5 * (fahrenheit - 32)) / 9);
		assertEquals(-24, celsius);
	}

	@Test
	public void testFahrenheitBoundaryIn() {
		int fahrenheit = 0;
		int celsius = (int) ((5 * (fahrenheit - 32)) / 9);
		assertEquals(-17, celsius);
	}

	@Test
	public void testFahrenheitBoundaryOut() {
		int fahrenheit = 100;
		int celsius = (int) ((5 * (fahrenheit - 32)) / 9);
		assertEquals(37, celsius);
	}

}
